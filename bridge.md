# Qemu bridge networking

It is possible to use qemu-bridge-helper to bridge a VM directly to an
ethernet interface on the host machine. This is useful to make a VM reachable
from anywhere on the LAN.

There are a couple of things that need to be configured for this to work.

First create a bridge and set it to up, then add the ethernet interface to it
```
# ip link add name br0 type bridge
# ip link set br0 up
# ip link set enp3s0 master br0
```
These steps can also be added to `/etc/rc.local` so they are applied on boot.

The next step is to assign an ip to the newly created bridge. This can be
done using a DHCP client daemon or a static IP configuration.
Note that you must also configure `/etc/resolv.conf` on the host system and
set up the routes properly.

On runit based distros, copy `/etc/runit/sv/dhcpcd` to `/etc/runit/sv/dhdpcd-br0`
and change the `/etc/runit/sv/dhcpcd-br0/conf` to
```
IFACE="br0"
OPTS=""
```
then enable dhcpcd-br0 using
```
# ln -s /etc/runit/sv/dhcpcd-br0 /run/runit/service
```
Using DHCP to configure the bridge has the benefit of automatically setting up
the correct default routes and adding DNS resolvers to `/etc/resolv.conf`.
Make sure there is no DHCP client trying to configure `enp3s0`.

Create the ACL used by qemu-bridge-helper to determine if it is allowed to add
its `tap` interface to `br0`
```
# mkdir -p /etc/qemu
# echo 'allow br0' >> /etc/qemu/bridge.conf
```

Finally, create a netdev in qemu that uses the bridge helper to create a tap
device and connect it to the bridge, for example
```
#!/bin/sh
qemu-system-x86_64 -enable-kvm -smp cores=1 -m 256M \
    -hda obsd.qcow2 \
    -netdev bridge,id=nd0,br=br0 -device virtio-net,netdev=nd0,mac=52:54:00:12:34:59 \
    -display gtk
```
